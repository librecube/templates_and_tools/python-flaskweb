# python-flaskweb

This Python project let's you set up a small web application in no time. It
is based on [Flask](https://palletsprojects.com/p/flask/) and includes
navigation bar, drag-and-drop file uploading, bootstrap templates, and so on.

## Getting Started

Clone or download the repository and start it:

```bash
$ git clone https://gitlab.com/librecube/lib/python-flaskweb myWeb
$ cd myWeb
$ pip install -r requirements.txt
$ export FLASK_APP=flaskweb
$ export FLASK_DEBUG=1
$ flask run
```
