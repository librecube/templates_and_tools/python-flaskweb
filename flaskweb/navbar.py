from flask_nav.elements import Navbar, View, Text, Subgroup


def top_nav():
    navbar = Navbar(View('FlaskWeb', 'home.index'))
    navbar.items.append(Text('UTC'))

    subgroup = Subgroup('Subgroup')
    navbar.items.append(subgroup)
    subgroup.items.append(View('View', 'home.index'))
    subgroup.items.append(Text('Text'))

    return navbar
